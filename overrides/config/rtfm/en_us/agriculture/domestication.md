Horses, donkeys, and llamas can be used for work. They can pull carts, till the land, and even process raw materials!

To make them pull a cart, first craft a cart and place it in the world. After mounting the tamed work animal, press "R" (this keybind can be changed) when near the cart to attach the cart to the animal. The animal will pull the cart when rode or lead with a lead. The cart's inventory can be opened by shift-right-clicking the cart.

Plows work similarly. Right-click to engage and disengage the plow. The plow will till dirt and grass into farmland and coarse dirt into dirt (and then farmland). They first must be equiped with a hoe (or three). You can also equip the plow with shovels to make path blocks instead of farmland.

They can also operate presses and grindstones. Simply attach the animal to the device with a lead and, assuming that there is enough room, they will begin to work. This will save you from having to manually jump on grapes or grind wheat by hand.