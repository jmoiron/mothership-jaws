Bottling is a blanket term to describe the creation and bottling of cider, wine, and olive oil. Apples and cider apples produce apple juice, then cider. Grapes, wildberries, and ironberries produce their various juices and wines. Olives produce olive oil.

To begin, you must crush the fruit. An industrial squeezer is the most effective way, but a horse press or a crushing tub also works.

Second, you need to put the juice in a brewing barrel. After waiting for awhile, it will become wine or cider, depending on what your juice was. With olive oil, you can skip this step.

Finally, you need to bottle your brew. You can do this by hand. Afterward, you will need to craft the filled bottle with a piece of paper to make it sellable. Alternatively, you can use a bottling machine which will remove the need for paper and crafting.

That said, there is a shortcut you can take. You can take grapes, berries, or cider apples and put them into an industrial fermeneter, turning them directly into their alcoholic counterparts. This is a lot faster and easier than crushing the fruit and putting the juice in barrels, but there are two drawbacks. The first is that you can only use cider apples, berries, and grapes. Regular apples will just become ethanol. The second is that people will insult the quality, but who cares what they say! People can be such snobs when it comes to alcohol. I say, slap a label on it and it is all the same.