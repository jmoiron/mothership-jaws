Some have speculated that it is technically possible to eat only one food forever and be perfectly fine, but that the human mind simply cannot handle such monotony, leading to starvation. Due to this, a diverse diet is important.

This really isn't that hard to achieve. In addition to traditional crops like potatoes, wheat, and carrots, there are wild, farmable roots like core, marsh mallow, and ginseng. Not only that, you can acquire eveything from boudin noir to wah by trading with the towns that dot the landscape, so don't complain!

The more foods you try, the higher your max health will be. Try to eat all 106 foods for the highest max health possible! The "Food Book" will help you keep track of what you have ate and haven't.