It is possible to breed just about every crop in the game, improving its stats until you've created a super-crop. The seed analyser and the "Agricultural Journal" will help you keep track of this process.

When two or more seeds are planted on crop sticks around an empty double crop stick, there is a chance that they will crossbreed, resulting in a better plant in the center. There are many different arrangements that work, so experiment around with it and see what works best!

The use of irrigation channels and sprinklers speeds up crop growth. One sprinkler provided with water covers a 7x7 area, removing the need for a water source block. While rain does fill irrigation tanks, you'll likely need some other way of gathering water, discussed in the "Resources" tab.

You can also use the garden cloche to grow nearly any crop once it has been analysed. Better crops produce more produce when planted in the cloche.