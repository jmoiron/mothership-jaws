Trade is wonderful thing. You can sell stuff you produce easily to gain materials that you cannot gain so easily, if at all. By pressing "M" (adjustable in the controls section) you can open the Millenaire menu, which will tell you in-depth about the different civilizations you can encounter.

Most towns will buy food, especially exotic foods that aren't native to their land. Wine and cider are also popular sellers. Iron sells well, and some towns also buy gold. Nearly all towns will buy building materials too, like wood and stone.

With the money you gain in trade you can buy all sorts of cool things. Some towns sell clay. You can't ever have too much clay! You can buy fine drink and food that is otherwise unobtainable. They can also sell you banner patterns and other kinds of artwork.

Note that trade isn't infinite. You can only buy what the town has on hand, and you can only sell until their storage is all filled up, so keep that in mind.

If you are playing with other players, you can trade with them as well. Hitting "V" while looking at another player will open a trade screen. You can also craft and configure vending blocks to set up shops to do business with players even when you aren't online.