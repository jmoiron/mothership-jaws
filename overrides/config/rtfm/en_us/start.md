Feeling a bit lost? Here's an easy way to start. First, craft a saddle because they are craftable now. Afterwards, go and explore. You are looking for a horse, but other stuff you see will be mapped by VoxelMap and that information might be useful later. Once you find a horse (or other pack animal), tame it and ride it. Congratulations! You have your first mode of transportation.

Once you've found a good spot to settle down, set up a pen for the horse and get some farms going (see the "Agriculture" section). Hopefully you've gathered some clay at this point for grout. Make either a melter and an alloy kiln or a smeltery. Now you can process ores (without your hammer), make basic alloys, and cast tool parts.

You'll definitely want to make a little batch of either brass or bronze to make a couple bushings. These can be used for wheels for a horse cart, your first transportation upgrade! You'll also want to make a hammer and a lumberaxe to speed up mining and logging. Finally, you can cast a couple bronze gears to make a rolling machine and start crafting some materials for light rail (see the "Transportation" section).

The next step is treated wood for railbeds. Set up a coke oven and start tossing coal and wood in there for creosote. You can treat the wood with the creosote and make rail ties, among other things. Now you've got track!

Of course, there's lots of other things to do, but this will get you started. Eventually you'll want to set up steel production and better ore processing (see the "Industry" section). You'll also want to interact with the various towns (see the "Civilization" section) and even start building bigger and better transport lines between towns, mines, your base, and etc. The sky is the limit!
