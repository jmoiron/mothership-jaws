# Transportation
- [Automobiles](transportation/land.md)
- [Airplanes](transportation/air.md)
- [Light rail](transportation/light.md)
- [Heavy rail](transportation/heavy.md)

### Further reading
[Immersive Railroading Wiki](https://github.com/cam72cam/ImmersiveRailroading/wiki)
[Railcraft Wiki](https://railcraft.info/wiki/)
MTS Manual (Immersive Vehicles)