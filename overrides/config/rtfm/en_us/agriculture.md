# Agriculture
- [Nutrition](agriculture/nutrition.md)
- [Domestication](agriculture/domestication.md)
- [Hybridization](agriculture/hybridization.md)
- [Bottling](agriculture/bottling.md)

### Further reading
Almanac (Rustic)
Agricultural Journal (Agricraft)
Food Book and Journal (Spice of Life, both editions)