# Resources
- [Reservoirs](resources/reservoirs.md)
- [Quarries](resources/quarry.md)
- [Ores and Rough Ores](resources/rough.md)

### Further reading
Field Manual (Geolosys)