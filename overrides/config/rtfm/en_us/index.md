![Mothership: Just Another Whistle Stop](menu:msjaws_sm.png)
-----
[Discord](https://discordapp.com/invite/c5De6cc)
-----
All aboard! This is The Mothership's fork of Just Another Whistle Stop!

Just Another Whistle Stop is a modpack built around all things that move. We've got everything from puny horse carts to giant locomotives! You'll need cars, trucks, planes, and trains to solve the logistical challenges posed by this pack. There are biomes to cross, towns to connect, deposits to be mined, and oil to be extracted. This pack has all the great transportation mods like Railcraft, Immersive Railroading, and Immersive Vehicles (formerly MTS) while also using mods like Geolosys, Open Terrain Generator, and Millenaire to create interesting transport scenarios. Get to it and build a transportation network like never before!
-----
[Technical Information](info/version.md)
[Recommended Settings](info/settings.md)
[Frequently Asked Questions](info/faq.md)
