1.3.8

New to this version is Buildcraft|Transport, which adds buildcraft
pipes.  If you've never used buildcraft pipes before, check out
the tutorial:

  https://minecraftbuildcraft.fandom.com/wiki/Tutorials/Pipes_for_Beginners

These have been added because the pack lacks many item transfer options
that have controlled backpressure.  This leads people to use hoppers, which
have backpressure but can be very expensive on the server.

added mods:
- added buildcraft|transport (pipes)
- added buildcraft|compat (JEI for fuels etc)
- added cosmetic armor reworked

fixes:
- enabled buildcraft redstone engine (for pipes)

updated mods:
- AppleCore-mc1.12.2-3.3.0.jar
- Construct's Armory 1.2.5.9
- CraftTweaker2-1.12-4.1.20.574.jar
- Golden Airport Pack [Immersive Vehicles] 1.2.7.jar
- IndustrialRenewal_1.12.2-0.17.6.jar
- LittleTiles_v1.5.0-pre199_32_mc1.12.2.jar
- millenaire-8.1.0.jar
- UNU Civilian Pack [MTS] 1.12.2-18.0.0-3.5.0.jar
- UNU Parts Pack [MTS] 1.12.2-18.0.0-3.5.0.jar

1.3.7

added mods:
- added bibliocraft
- added immersive vehicles "Golden Airport Pack"
- added construct's armory
- added signpost (waystones/teleportation is disabled)
- added kiwi mod

fixes: 
- added recipes for nether and ender hive
- fixed tinkers tool leveling config
- reduced pam's garden spread significantly
- minecolonies:
  - changed to dynamic colony size, allow infinite sized colonies
  - removed colony grief protection (claim chunks for anti-griefing)

updated mods:
 - CraftTweaker2-1.12-4.1.20.573.jar
 - CreativeCore_v1.10.14_mc1.12.2.jar
 - CyclopsCore-1.12.2-1.6.4.jar
 - Vehicles-1.12.2-18.2.0.jar
 - LittleTiles_v1.5.0-pre199_28_mc1.12.2.jar

There's also a fix for a client crasher for MrCrayfish's Gun mod:
- https://www.dropbox.com/s/38p5n5581z32j03/guns-0.15.3-1.12.2.jar?dl=0

Cannot re-distribute via curse for the pack, people will have to get it manually for now.

1.3.6

- added phosphor
- added chineseworkshop
- added mccaws's roofs
- added notenoughroofs
- added JEI bees & JEI integration
- removed mo' creatures & custom mob spawner
- added recipes for slate and marble

- updated mods:
  - CreativeCore_v1.10.13_mc1.12.2.jar
  - engineersdecor-1.12.2-1.0.20.jar
  - ImmersivePosts-0.2.1.jar
  - Immersive Vehicles-1.12.2-18.1.4.jar
  - IndustrialRenewal_1.12.2-0.17.5.jar
  - LittleTiles_v1.5.0-pre199_27_mc1.12.2.jar
  - StorageDrawers-1.12.2-5.4.2.jar
  - randompatches-1.12.2-1.21.0.3.jar
  - rustic-1.1.3.jar

1.3.5

- added the mighty architect
- added magneticraft
- added redstone gauges and switches
- added floodlights
- added fairy lights
- added nomorerecipeconflict
- changed logo & background

1.3.4

- added chunkpregenerator to manifest
- removed a bunch of configs from mods that were removed
- added Industrial Renewal
- added Xtones
- disabled Ent spawning

1.3.3

updated mods:

- buildcraft-builders-7.99.24.6.jar
- buildcraft-core-7.99.24.6.jar
- buildcraft-energy-7.99.24.6.jar
- BuildingGadgets-2.8.3.jar
- Chisel-MC1.12.2-1.0.2.45.jar
- CTM-MC1.12.2-1.0.2.31.jar
- CreativeCore_v1.10.10_mc1.12.2.jar
- ImmersiveRailroading-1.7.3_1.12.2.jar
- Vehicles-1.12.2-18.1.1.jar
- InventoryTweaks-1.64+dev.151.jar
- justenoughdimensions-1.12.2-1.6.0-dev.20200416.184714.jar
- jei_1.12.2-4.15.0.293.jar
- LittleTiles_v1.5.0-pre199_19_mc1.12.2.jar
- minecolonies-1.12.2-0.11.804-RELEASE-universal.jar
- TConstruct-1.12.2-2.13.0.183.jar
- UNU Civilian Pack [MTS] 1.12.2-18.0.0-3.4.2.jar
- UNU Parts Pack [MTS] 1.12.2-18.0.0-3.4.1.jar

VanillaFix held back due to incompatibilities.

1.3.2
- added chunk pregenerator to server files
- modified forge.cfg to ignore cascading log messages during pregen (millenaire produces a lot of these)
- modified server.properties to allow flight by default and set world gen to OTG + Biome Bundle by default

1.0.0
- forked for Mothership Jaws
- switch from VoxelMap to JourneyMap
- added MoCreatures, Archaeology
- and many other mods (many of which were eventually removed)

0.5.2
-Mod updates
-Added  MalisisDoors, MalisisCore, Cloche Compat Mod, and Nature's Compass
-Replaced Chunk Claim with Claimit for server installations
-Tweaked the fuel values for the high oven
-Readded a few removed mixer recipes
-Raised stack size for Millenaire coins
-Added recipe for raw firestone
-Tweaked manual recipe
-Added recipe for ancient tome (Minecolonies)
-Removed Quark's iron ladder (use IE's or ED's version)

0.5.1
-Mod updates
-Fixed crushed obsidian recipe
-Tweaked font size on stack numbers
-Added Random Patches, Trade, Architecturecraft, Vending Blocks, FoamFlower, Schematica, LunatriusCore, Reauth, and DEUF
-Added Chunk Claim as an optional mod for servers
-Ores can now spawn in sandstone
-Colonies now use dynamic sizing
-Fixed "divide by zero" crash
-Added recipe for shader grab bags
-Tweaked the manual

0.5.0
-Added Little Tiles, Creative Core, Chisel, and Connected Textures as optional mods
-Added Placebo, Toast Control, Apple Skin, Just Enough Dimensions, Metal Chests, and Vanilla Vistas
-Removed the nether again (this time for real!)
-Tweaked stack sizes. Most items now stack to 32 but there are some major exceptions, like ore.
-Raised the stack sizes for a few millenaire blocks to 256 to help Millenaire villages with storage. Note that existing villages will likely not benefit unless you buy out their stock of path blocks, thatch, mud brick, timber frames, and etc.
-Added a way to turn blaze powder back into blaze rods at the metal press. You might not realize it, but this changes everything.
-The lunchbox now stacks up to 6
-Hid some stuff in JEI. Don't bother looking for it.
-Players now spawn with the One Probe, which was in the pack the whole time!
-Added crushed obsidian recipe
-Iron wine and berry wine can now be made in a fermenter
-Restored one of the weirder settings in Immersive Vehicles to 256.
-Added FAQ and start guide to the manual
-Disable some OP gadgets (sorry)
-Galena can now be found in snowy biomes
-Tweaked stone generation. Quark now handles more of it.
-Tweaked ore generation. Deposits are all somewhat smaller.
-Hopefully reduced lag significantly

0.4.0
-(Re)removed the end and shulker boxes
-Added Akashic Tome, Stackable, Custom Backrounds
-Tweaked olive oil recipe. You can no longer craft it at a table.
-Tweaked fermenting recipes for grapes and cider apples. They now use the "proper" fluid.
-Tweaked starting inventory. Players now start with toolbox and lunch box as well as the hammer. All the books now start in the tome.
-Implemented the manual
-Implemented the custom main menu

0.3.0
-Added Voxelmap, Mousetweaks, Inventorytweaks, Engineer's Decor, Engineer's Doors, Alternating Flux, Biome Bundle, Diet Hoppers, and Custom Main Menu
-Removed iron rod, chain, from quark
-Removed brick wall from ceramics
-Tweaked geolosys. Diamonds, emeralds, and uranium will now spawn in mountains in addition to mushroom biomes
-Spice of Life now uses number of foods eaten instead of amount of hunger filled
-Changed mulch recipe


0.2.0
-Intitial beta
