Q: How do I make steel?

A: Use the high oven, or later the arc furnace. See the steelmaking page under the industry tab for more info.

Q: What do I do with rough ores?

A: Put them in the crusher. They are useless until you have a crusher, but you should save them anyway because you'll get an ore cluster for roughly every 3 crushed.

Q: Where are the ores?

A: They are underground deep beneath samples that spawn on the surface. See the "Field Manual"

Q: Why does the world crash when I try and go to the nether?

A: The nether is disabled. You shouldn't even be able to make a portal, but there is a bug at the moment.

Q: What biomes are considered, from a technical standpoint, spooky?

A: Roofed forests, mainly. Biome Bundle's overworld caverens are also considered spooky.

Q: How do I make water?

A: Use a Railcraft water tank or gather rain in an Agricraft water tank.

Q: Why won't the villagers buy X?

A: They either don't need it or they are out of storage space. They can only buy so much!

Q: AAAAH! WHY IS EVERYTHING IS ON FIRE?

A: Take the firestone out of your inventory and put it somewhere that's not flammable.

Everything else is answered somewhere in this manual or in the material it references, so please look there first if you have any questions. Failing that, try google. Only after doing that should you go and ask on the discord.