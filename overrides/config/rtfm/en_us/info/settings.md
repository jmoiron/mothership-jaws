Recommended world generator: Flatlands is the easiest for building while Biome Bundle is the hardest. That said, Biome Bundle has more interesting terrain. Vanilla Vistas is in the middle.

Recommended ram: 6 or higher. Players using less may need to remove the addons for Immersive Railroading and Immersive Vehicles or even those mods themselves.

Recommended pregen: 100, though the effect may be minimal on better systems.

Recommended render distance: Depends on the system. You will likely run into lag if you don't reduce render distance while using certain vehicles like planes.

It should be noted that Optifine is NOT supported and is known to cause strange graphical glitches and errors. Use at your own risk!
