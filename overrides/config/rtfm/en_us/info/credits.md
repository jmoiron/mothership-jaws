The original credits for Just Another Whistle Stop.  We'd like to acknowledge and thank the original author of this pack, Profilename3.

[Quark and dependency is a mod by Vazkii.](https://www.curseforge.com/minecraft/mc-mods/quark)
[Akashic Tome is also a mod by Vazkii.](https://www.curseforge.com/minecraft/mc-mods/akashic-tome)
[BetterFPS is a mod by Guichaguri.](https://www.curseforge.com/minecraft/mc-mods/betterfps)
[Ceramics is a mod by KnightMiner.](https://www.curseforge.com/minecraft/mc-mods/ceramics)
[Inspirations is a mod by KnightMiner.](https://www.curseforge.com/minecraft/mc-mods/inspirations)
[Tinkers' Complement is a mod by KnightMiner.](https://www.curseforge.com/minecraft/mc-mods/tinkers-complement)
[Millenaire is a mod by Kinniken, Zoythrus, Orange1861, 073theboss, and FireController1847](https://millenaire.org/)
[Pam's Portal Poof is a mod by Pamela Collins and Rhodox](https://www.curseforge.com/minecraft/mc-mods/pams-portal-poof)
[Railcraft has a license and a wiki and a blog and wants you to be aware of that, so here](http://railcraft.info)
[VoxelMap is a mod by MamiyaOtaru.](https://www.curseforge.com/minecraft/mc-mods/voxelmap)
[Carry On is a mod by  Tschipp and Purplicious_Cow.](https://www.curseforge.com/minecraft/mc-mods/carry-on)
[D&RGW Rolling Stock Pack is a pack by Peyton Smith](https://www.curseforge.com/minecraft/texture-packs/d-rgw-narrow-gauge-pack-ir)
Engineer's Doors is a mod by Nihiltres.

Special thanks to [Derwin Castillo](http://www.derwincastillo.com/) who created the "Just Another Whistle Stop" logo. 

All mods are licensed under their respective licenses. They can be found and downloaded individually on curseforge. Their licenses can be found on their respective curse pages. Should you have any issue with the usage of your mod in this pack or how this document has been compiled, please contact Profilename1 on his [Discord.](https://discordapp.com/invite/c5De6cc)
