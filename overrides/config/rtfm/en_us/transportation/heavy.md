Heavy rail is rail and machinery that must be made with large equipment such as the casting basin and the plate machine. These can be placed with the Immersive Railroading manual. Shift-clicking while looking at the air cycles the machine being placed. Right-clicking places the machine down. Clicking the machine with the large wrench creates the multiblock.

The main benefit heavy rail provides over light is power. The larger trains and stock can move more material faster than their lighter cousins.

That said, there are drawbacks. For one, both steam and diesel heavy rail engines require time to heat up before they can go. There also isn't as much opprotunity for automation of heavy rail as there is light rail.

It is also expensive. Just to build the equipment, you are looking at the following:

Stone brick - 114
Blast brick - 86
Sand - 126
Piston - 1
Stone slab - 24
Block of steel - 2
Steel scaffolding - 197
Heavy engineering block - 79
Light engineering block - 138

On top of that, you will still need dozens of blocks of steel (if not more) to build a decent line, although smaller gauges such as "Minecraft" and "Narrow" require less material than the standard gauge.