Automobiles are a very effective form of transportation, discussed in depth in the MTS Manual. It should be noted that it might be filled with UNU propoganda if you have that module installed.

Diesel engines can run on diesel or biodiesel, while gasoline engines can run on fuel, gasoline, and ethanol.

Offroading is possible to an extent but it can be very dangerous. Roads and paths are recommended so that you can go top speed with little trouble. Be sure to keep an eye on your minimap if you do try to offroad, as it can warn you of upcoming obstacles.