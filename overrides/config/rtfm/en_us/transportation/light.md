Light rail is rail and machinery that does not require large equipment like heavy rail does. Light rail only requires the use of a rolling machine.

The main benefit light rail has over heavy is that it is much less expensive to build. Many things can be made with iron or even bronze instead of steel. There are also plenty of tools for automation, like locking rails which pair greatly with fluid and item loaders. On top of that there are messenger tracks, launcher tracks, primer tracks, and all sorts of other exciting devices.

Of course, light rail tends not to be as fast or powerful as heavy rail, but the use of high speed track does help with this.