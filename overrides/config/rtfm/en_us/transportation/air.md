Airplanes are an incredibly effective yet dangerous form of transportation, discussed in depth in the MTS Manual.

Airplanes only run on fuel, which is gasoline or ethanol that has been mixed with lead.

Should you decide to fly it is recommened that you build a runway no smaller than 300 blocks by 30 blocks, if not bigger. You will also want to mark waypoints at both ends of your runway on your minimap, as well a point or two approaching the runway to help you guide your landing.

You also might want to make a parachute. If you set it to AUTO it will deploy if you hold it in your hand while falling. Just in case, right?