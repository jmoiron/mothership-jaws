There are three kinds of reservoirs: Water, oil, and lava.

Oil reservoirs are only found in sandy biomes, like deserts.

Lava reservoirs are only found in cold biomes, like ice plains.

Water reservoirs are only found in wet biomes, like swamps.

To see if a specific chunk has a reservoir, you need to drill with a core sample drill. It will give you a sample, showing what fluids, if any, are present. If there is a reservoir, it can be pumped by a pumpjack built in the same chunk.