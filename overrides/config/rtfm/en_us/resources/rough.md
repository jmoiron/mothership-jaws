Ores spawn in large, biome specific deposits. What ores spawn where is discussed in the "Field Manual".

Ore samples spawn on the surface. Somewhere beneath them, the rest of the deposit spawns. About 1/4 of the deposit will go ahead and give you a cluster when mined. The rest is rough ore. You should get a cluster from these for roughly every three you grind in a grinder.

In other words, you get half the metal right away and the other half when you grind up all the rough ore.