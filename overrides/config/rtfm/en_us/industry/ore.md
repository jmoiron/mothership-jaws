When you first start out, the only way you have to process ore is a hammer. You can use it to crush ore clusters you mine into dust, which can be smelted in a furnace to make ingots.

The step up from this is the melter, which allows you to process the clusters directly into molten metal, which can be cast into blocks, ingots, and even tool parts. However, you still only get one ingot out of each cluster.

The smeltery and the high oven are both just oversized version of the melters, using either liquid or solid fuels. They can reach higher temperatures and can smelt the ore more efficiently, yielding an ingot and a half worth of metal out of each cluster.

The blast furnace, however, is even more efficient. It produces two ingots in addition to some slag. It can only be powered by charcoal or coke coal. The arc furnace is similar but is powered by RF.

The crusher is the best device for ore processing. It gets two ingots worth of dust out of each ore cluster and can also sometimes produce an extra dust of a similar type depending on the ore. It is also the only way to get ore clusters out of rough ores.