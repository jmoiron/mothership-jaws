After seting up steel production and RF production, you can start in on liquid fuels. There are two paths you can take: You can drill for oil and refine it, or you can grow certain crops and process them into various fuels.

To make fuels from plants you will first need a fermenter, which can turn various crops into ethanol (maize is best). Ethanol doesn't burn very efficiently, but it can be used in portable generators to make RF or in gasoline automotive engines.

With a mixer, you can enrich the ethanol with lead grit to make fuel, which burns much more efficiently than regular ethanol (or even gasoline). In addition to gasoline engines and portable generators, it can be burned in airplane engines and the combustion engines required for quarries.

With a squeezer, you can make plant oil from seeds (hemp seeds are best). This can then be mixed in a refinery to make biodiesel, which can be burned in a diesel generator or in diesel automotive engines.

Alternatively, you can drill for oil, which is only found in certain biomes (discussed in the "Resources" tab). You can use the core sample drill to check for fluid deposits and the pumpjack to pump those deposits. The distillation tower can turn the oil into diesel and gasoline among other things. Diesel can be used similarly to biodiesel and gasoline can be used similarly to ethanol, and they tend to be more efficient than their plant-based counterparts.