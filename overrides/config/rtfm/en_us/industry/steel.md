Steel is the lifeblood of heavy industry. You will need steel for everything from generators to presses to railcars, so you will need a good setup for making lots and lots of steel.

Steel must be made in either a high oven (described in "Materials and You") or later in an arc furnace (described in the "Engineer's Manual"). Since the latter of these requires steel to make, you will have to start with the high oven.

The high oven must be fueled with either charcoal or coke coal. Once it heats up, it will be able to melt iron. You can then add additives to make the iron into molten steel.

You will need three additives:

Either sand or red sand

Either aluminum or redstone dust

Either gunpowder, nitrate dust, or sulfur

The arc furnace works similarly but runs on RF, needs coke dust as an additive, and is a little stricter on the additives it will accept.

Sand can be found out in the world or can be obtained by grinding gravel in a grinder. Aluminum and redstone dust can be obtained by mining bauxite and cinnabar deposits, respectively. Gunpowder can be obtained by killing creepers, while sulfur can be found while mining coal deposits. Nitrate dust can be made by grinding sandstone.

It might be worthwhile to setup a crusher and metal press to craft and grind sandstone to mass produce nitrate dust.