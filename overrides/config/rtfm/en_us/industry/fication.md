Portals? What are you talking about? There are no lands under the bedrock or over the sky! Such is just silly superstition, perhaps even heresy. Alchemy, though, that's for real.

Over the centuries, two strange and amazing processes have been discovered. The first is netherfication. By taking certain items and pouring lots of lava over them using a casting basin, they take on new and magical properties!

Treated sticks become blaze rods.
Cobblestone becomes netherrack.
Sand becomes soul sand.
Grapes become nether warts.
Iron nuggets become ghast tears.
Iron dust becomes glowstone dust.
Slime balls become orange slime balls.
Quartz becomes firestone, which, when cut and refined, can be tossed into lava to absorb its power. It can then be used in anything that accepts solid fuel, though it will need to be recharged. Be careful, though, as raw firestone is incredibly dangerous.

Enderfication is similar, but uses that that holiest of elements silver instead.

Apples become popped chorus fruit.
Cobblestone becomes end stone.
Slime balls become green slime balls.