Steam is a gas produced by boiling water. To create steam, you will need to build a steam boiler. A boiler needs a firebox and a tank.

There are two types of fireboxes and two types of tanks: High and low pressure tanks, liquid and solid fireboxes.

While low pressure tanks require iron plates and can produce a maximum of 10mb of steam per tick, high pressure tanks require steel plates and can produce twice as much steam. In fact, a full-size high pressure boiler produces enough steam to max out a steam turbine.

Solid fireboxes consume nearly all kinds of solid fuel (like a furnace). Liquid fireboxes consume some (but not all) liquid fuels, like gasoline and creosote oil.

Boilers can be made in various sizes, with the smallest being a single firebox with a single tank on top of it and the biggest possible being a 3x3 square of fireboxes with four 3x3 rows of tanks on top of it.

Steam itself serves two purposes. By building a steam turbine and steam turbine housing, you can produce around 900 RF/t provided that you have enough steam. Steam can also be used as fuel in a smeltery to melt certain softer metals like lead, zinc, and tin.

Some vehicle run on steam. That is discussed in the transportation tab.