Storage is easy. Just look at a good spot with the item in your hand and hit "P" to start a stack (that keybind is adjustable). Right click adds items and left click subtracts.

Another interesting choice is to use metal chests. They are similar to regular chests but made out of metal.

Of course, on top of that you've got your plain old chests and the silos, tanks, and barrels discussed in the "Engineer's Manual".

That said, there are more unconventional options worth considering. If you have access to heavy machinery required to produce railcars, you could lay down a short piece of track inside your base and put down a boxcar to use as storage. A "Brunel" gauge 70t hopper has as much storage space as 6 chests! Of course, it's a lot bigger than 6 chests, but this way you get it all in one UI.