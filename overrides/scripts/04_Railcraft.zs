//removed stuff
recipes.removeByRecipeName("railcraft:ingot#5$3");
recipes.removeByRecipeName("railcraft:ingot#9$3");
recipes.removeByRecipeName("railcraft:concrete$3");
recipes.removeByRecipeName("railcraft:concrete$2");
recipes.removeByRecipeName("railcraft:ingot#7$3");
//tie fix
recipes.removeByRecipeName("railcraft:tie#0$1");
recipes.addShaped(<railcraft:tie:0>,
 [[null, null, null],
  [null, null, null],
  [<ore:slabTreatedWood>, <ore:slabTreatedWood>, <ore:slabTreatedWood>]]);
//adding ingot to plates
recipes.addShapeless(<railcraft:plate:11>, [<railcraft:ingot:9>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<railcraft:plate:10>, [<ore:ingotZinc>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<railcraft:plate:9>, [<railcraft:ingot:7>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<railcraft:plate:6>, [<railcraft:ingot:5>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<railcraft:plate:2>, [<ore:ingotTin>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<contenttweaker:tin_grit>, [<ore:oreTin>, <immersiveengineering:tool:0>]);
recipes.addShapeless(<contenttweaker:zinc_grit>, [<ore:oreZinc>, <immersiveengineering:tool:0>]);
//crushed obby
recipes.addShapeless(<railcraft:generic:7>, [<railcraft:dust:0>, <railcraft:dust:0>, <railcraft:dust:0>, <railcraft:dust:0>]);
