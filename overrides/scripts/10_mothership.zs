
recipes.addShaped(<minecraft:dragon_egg>,
 [[<railcraft:generic:7>, <railcraft:generic:7>, <railcraft:generic:7>],
  [<railcraft:generic:7>, <minecraft:egg>, <railcraft:generic:7>],
  [<railcraft:generic:7>, <railcraft:generic:7>, <railcraft:generic:7>]]);

recipes.addShaped(<minecraft:clay> * 8, 
 [[<minecraft:sand>, <minecraft:dirt>, <minecraft:sand>],
  [<minecraft:dirt>, <minecraft:water_bucket>, <minecraft:dirt>],
  [<minecraft:sand>, <minecraft:dirt>, <minecraft:sand>]]);

recipes.addShaped(<minecraft:clay> * 4, 
 [[<minecraft:sand>, <minecraft:gravel>, <minecraft:sand>],
  [<minecraft:gravel>, <minecraft:water_bucket>, <minecraft:gravel>],
  [<minecraft:sand>, <minecraft:gravel>, <minecraft:sand>]]);
  
# hide all unlimited distance remotes from SSN
mods.jei.JEI.removeAndHide(<storagenetwork:remote:1>);
mods.jei.JEI.removeAndHide(<storagenetwork:remote:2>);
mods.jei.JEI.removeAndHide(<storagenetwork:remote:3>);

# remove broken solar panels from industrial renewal
mods.jei.JEI.removeAndHide(<industrialrenewal:solar_panel>);
mods.jei.JEI.removeAndHide(<industrialrenewal:solar_panel_frame>);

# kill conarm knapsack
mods.jei.JEI.removeAndHide(<conarm:travel_sack>);

# add in recipes for slate
recipes.addShaped(<rustic:slate> * 32, [
    [<ore:dustAsh>, <minecraft:clay>, <ore:dustAsh>],
    [<minecraft:clay>, <minecraft:coal_block>, <minecraft:clay>], 
    [<ore:dustAsh>, <minecraft:clay>, <ore:dustAsh>]]);

recipes.addShaped(<rustic:slate> * 8, [
    [<ore:dustAsh>, <minecraft:clay_ball>, <ore:dustAsh>],
    [<minecraft:clay_ball>, <minecraft:coal>, <minecraft:clay_ball>],
    [<ore:dustAsh>, <minecraft:clay_ball>, <ore:dustAsh>]]);

# add in a recipe for marble
recipes.addShaped(<quark:marble> * 8, [
    [<ore:dustAsh>, <minecraft:stone>, <ore:dustAsh>],
    [<minecraft:stone>, <minecraft:dye:15>, <minecraft:stone>], 
    [<ore:dustAsh>, <minecraft:stone>, <ore:dustAsh>]]);

# add recipes to create nether & end hives, which do not gen in overworld
# they both require bee output, to require people to initiate foray into
# bees by going out and collecting from naturally occuring hives

recipes.addShaped(<forestry:beehives:4>, [
    [<minecraft:obsidian>, <minecraft:obsidian>, <minecraft:obsidian>],
    [<forestry:propolis>, <forestry:apiary>, <forestry:propolis>],
    [<minecraft:obsidian>, <minecraft:purpur_block>, <minecraft:obsidian>]]);

recipes.addShaped(<extrabees:hive:2>, [
    [<minecraft:stone_slab:6>, <minecraft:stone_slab:6>, <minecraft:stone_slab:6>],
    [<forestry:phosphor>, <forestry:apiary>, <forestry:phosphor>],
    [<minecraft:nether_brick>, <minecraft:nether_brick>, <minecraft:nether_brick>]]);
