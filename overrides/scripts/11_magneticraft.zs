
# allow conversion between magneticraft limestone variants
mods.chisel.Carving.addVariation("limestone", <magneticraft:limestone:0>);
mods.chisel.Carving.addVariation("limestone", <magneticraft:limestone:1>);

mods.jei.JEI.removeAndHide(<magneticraft:ores:0>);
mods.jei.JEI.removeAndHide(<magneticraft:ores:1>);
mods.jei.JEI.removeAndHide(<magneticraft:ores:2>);
mods.jei.JEI.removeAndHide(<magneticraft:ores:4>);
