val bag = <immersiveengineering:shader_bag>;
val bag1 = bag.withTag({rarity:"COMMON"});
val bag2 = bag.withTag({rarity:"UNCOMMON"});
val bag3 = bag.withTag({rarity:"RARE"});
val bag4 = bag.withTag({rarity:"EPIC"});
val bag5 = bag.withTag({rarity:"IE:MASTERWORK"});

mods.immersiveengineering.Blueprint.addRecipe("Grab Bags", bag1, [<millenaire:denierargent>]);
mods.immersiveengineering.Blueprint.addRecipe("Grab Bags", bag2, [<millenaire:denierargent>*2]);
mods.immersiveengineering.Blueprint.addRecipe("Grab Bags", bag3, [<millenaire:denierargent>*4]);
mods.immersiveengineering.Blueprint.addRecipe("Grab Bags", bag4, [<millenaire:denierargent>*8]);
mods.immersiveengineering.Blueprint.addRecipe("Grab Bags", bag5, [<millenaire:denierargent>*16]);

val blueprint = <immersiveengineering:blueprint>;
val grabprint = blueprint.withTag({blueprint:"Grab Bags"});
recipes.addShaped(grabprint,
 [[null, <millenaire:denierargent>, null],
  [<ore:dyeBlue>, <ore:dyeBlue>, <ore:dyeBlue>],
  [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>]]);