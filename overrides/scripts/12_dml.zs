# deep mob learning only allows you to skip to basic tier
# for all of its models;  we want to keep the models that
# it is possible to train at faulty tier but make the nether
# and end mobs craftable to basic

val fb = <forge:bucketfilled>;
val fuelBucket = fb.withTag({FluidName:"fuel_light", Amount:1000});
val fbtr = fuelBucket.transformReplace(<minecraft:bucket>);

recipes.addShaped(<deepmoblearning:data_model_wither>.withTag({tier:1}), [
    [<metalchests:metal_chest:5>, <metalchests:metal_chest:5>, <metalchests:metal_chest:5>],
    [<immersiveengineering:stone_decoration:3>, <deepmoblearning:data_model_wither>, <immersiveengineering:stone_decoration:3>], 
    [null, <minecraft:nether_star>, null]]);

recipes.addShaped(<deepmoblearning:data_model_dragon>.withTag({tier:1}), [
    [fbtr, <minecraft:dragon_egg>, fbtr],
    [<railcraft:boiler_firebox_solid>, <deepmoblearning:data_model_dragon>, <railcraft:boiler_firebox_solid>],
    [fbtr, <minecraft:dragon_egg>, fbtr]]);

recipes.addShaped(<deepmoblearning:data_model_wither_skeleton>.withTag({tier:1}), [
    [<minecraft:skull:1>, <minecraft:nether_star>, <minecraft:skull:1>],
    [<immersiveengineering:stone_decoration:3>, <deepmoblearning:data_model_wither_skeleton>, <immersiveengineering:stone_decoration:3>],
    [<minecraft:skull:1>, <minecraft:nether_star>, <minecraft:skull:1>]]);

recipes.addShaped(<deepmoblearning:data_model_ghast>.withTag({tier:1}), [
    [<deepmoblearning:living_matter_hellish>, <minecraft:ghast_tear>, <deepmoblearning:living_matter_hellish>],
    [<minecraft:quartz_block>, <deepmoblearning:data_model_ghast>, <minecraft:quartz_block>],
    [<deepmoblearning:living_matter_hellish>, <minecraft:ghast_tear>, <deepmoblearning:living_matter_hellish>]]);

recipes.addShaped(<deepmoblearning:data_model_blaze>.withTag({tier:1}), [
    [<ceramics:clay_hard:5>, <extrabees:honey_comb:30>, <ceramics:clay_hard:5>],
    [<quark:blaze_lantern>, <deepmoblearning:data_model_blaze>, <quark:blaze_lantern>],
    [<ceramics:clay_hard:5>, <extrabees:honey_comb:30>, <ceramics:clay_hard:5>]]);

# need a recipe for shulker shells since they are not obtainable
# can get purpur from chorus fruit via ET matter + apples
recipes.addShaped(<minecraft:shulker_shell>, [
    [<minecraft:purpur_block>, <minecraft:purpur_block>, <minecraft:purpur_block>],
    [<minecraft:purpur_block>, null, <minecraft:purpur_block>],
    [null, null, null]]);

recipes.addShaped(<deepmoblearning:data_model_shulker>.withTag({tier:1}), [
    [<minecraft:shulker_shell>, <extrabees:honey_comb:56>, <minecraft:shulker_shell>],
    [<minecraft:diamond_block>, <deepmoblearning:data_model_shulker>, <minecraft:diamond_block>],
    [<minecraft:shulker_shell>, <extrabees:honey_comb:56>, <minecraft:shulker_shell>]]);


