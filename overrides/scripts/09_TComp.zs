//High oven tweaks
import mods.tcomplement.highoven.HighOven;
HighOven.removeFuel(<minecraft:coal:1>);
HighOven.addFuel(<minecraft:coal:1>, 60, 2);
HighOven.removeFuel(<ore:blockCharcoal>);
HighOven.addFuel(<ore:blockCharcoal>, 600, 2);
HighOven.removeFuel(<ore:fuelCoke>);
HighOven.addFuel(<ore:fuelCoke>, 120, 4);
HighOven.removeFuel(<ore:blockFuelCoke>);
HighOven.addFuel(<ore:blockFuelCoke>, 120, 4);