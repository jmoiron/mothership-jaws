//no steel :O. Blast furnace doubles ore though now.
mods.immersiveengineering.BlastFurnace.removeRecipe(<immersiveengineering:metal:8>);
mods.immersiveengineering.BlastFurnace.removeRecipe(<immersiveengineering:storage:8>);
mods.immersiveengineering.ArcFurnace.removeRecipe(<immersiveengineering:metal:8>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:0>*2, <ore:oreCopper>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<minecraft:iron_ingot>*2, <ore:oreIron>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<minecraft:gold_ingot>*2, <ore:oreGold>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<railcraft:ingot:2>*2, <ore:oreTin>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:3>*2, <ore:oreSilver>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:2>*2, <ore:oreLead>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:1>*2, <ore:oreAluminum>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:4>*2, <ore:oreNickel>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<immersiveengineering:metal:5>*2, <ore:oreUranium>, 600, <immersiveengineering:material:7>);
mods.immersiveengineering.BlastFurnace.addRecipe(<railcraft:ingot:8>*2, <ore:oreZinc>, 600, <immersiveengineering:material:7>);
# steel arcfurnace recipe
mods.immersiveengineering.ArcFurnace.addRecipe(<immersiveengineering:metal:8>, <ore:ingotIron>, <immersiveengineering:material:7>, 400, 512, 
    [<ore:sand>, <ore:dustSaltpeter>, <immersiveengineering:material:17>, <immersiveengineering:metal:10>]);

# alternate to using aluminium, use redstone
mods.immersiveengineering.ArcFurnace.addRecipe(<immersiveengineering:metal:8>, <ore:ingotIron>, <immersiveengineering:material:7>, 800, 512, 
    [<ore:sand>, <ore:dustSaltpeter>, <immersiveengineering:material:17>, <minecraft:redstone>]);

# tungsten arcfurnace recipe
mods.immersiveengineering.ArcFurnace.addRecipe(<magneticraft:ingots:5>, <ore:ingotUranium>, <immersiveengineering:material:7>, 300, 512,
    [<ore:sand>, <ore:dustSaltpeter>, <minecraft:redstone>]);

mods.immersiveengineering.ArcFurnace.addRecipe(<railcraft:ingot:2>, <ore:dustTin>, null, 100, 512);
mods.immersiveengineering.ArcFurnace.addRecipe(<railcraft:ingot:8>, <ore:dustZinc>, null, 100, 512);
//crusher
mods.immersiveengineering.Crusher.addRecipe(<horsepower:flour>, <minecraft:wheat>, 512, <horsepower:flour>, 0.1);
mods.immersiveengineering.Crusher.addRecipe(<railcraft:dust:6>*3, <minecraft:ender_pearl>, 2048);
mods.immersiveengineering.Crusher.addRecipe(<contenttweaker:zinc_grit>*2, <ore:oreZinc>, 4096);
mods.immersiveengineering.Crusher.addRecipe(<contenttweaker:tin_grit>*2, <ore:oreTin>, 4096);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_autunite>, 2048, <geolosys:cluster:9>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_hematite>, 2048, <geolosys:cluster:0>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_bauxite>, 2048, <geolosys:cluster:6>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_cassiterite>, 2048, <geolosys:cluster:3>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_cinnabar>, 2048, <minecraft:redstone>, 1);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_galena>, 2048, <geolosys:cluster:5>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_gold>, 2048, <geolosys:cluster:1>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_lapis>, 2048, <minecraft:dye:4>, 1);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_limonite>, 2048, <geolosys:cluster:0>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_malachite>, 2048, <geolosys:cluster:2>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_sphalerite>, 2048, <geolosys:cluster:10>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_teallite>, 2048, <geolosys:cluster:3>, 0.33);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:gravel>, <contenttweaker:rough_coal>, 2048, <minecraft:coal>, 0.33);
//fermenter
val potion = <liquid:potion>;
val cheapwine = potion.withTag({Potion:"minecraft:awkward"});
val cheapcider = potion.withTag({Potion:"minecraft:thick"});
mods.immersiveengineering.Fermenter.addRecipe(null, <fluid:wine> * 250, <ore:cropGrape>, 80);
mods.immersiveengineering.Fermenter.addRecipe(null, <fluid:wine> * 250, <millenaire:grapes>, 80);
mods.immersiveengineering.Fermenter.addRecipe(null, <fluid:wine> * 250, <rustic:ironberries>, 80);
mods.immersiveengineering.Fermenter.addRecipe(null, <fluid:wine> * 250, <rustic:wildberries>, 80);
mods.immersiveengineering.Fermenter.addRecipe(null, <fluid:cider> * 250, <millenaire:ciderapple>, 80);
mods.immersiveengineering.Fermenter.addRecipe(null, <liquid:ethanol> * 120, <millenaire:maize>, 80);
//bottler
mods.immersiveengineering.BottlingMachine.addRecipe(<millenaire:oliveoil>, <minecraft:glass_bottle>, <liquid:oliveoil> * 1000);
mods.immersiveengineering.BottlingMachine.addRecipe(<millenaire:winebasic>, <minecraft:glass_bottle>, <fluid:wine> * 1000);
mods.immersiveengineering.BottlingMachine.addRecipe(<millenaire:cider>, <minecraft:glass_bottle>, <fluid:cider> * 1000);
//mixer
mods.immersiveengineering.Mixer.addRecipe(<liquid:fuel_light> * 250, <liquid:ethanol> * 250, [<ore:dustLead>] , 500);
mods.immersiveengineering.Mixer.addRecipe(<liquid:fuel_light> * 250, <liquid:gasoline> * 250, [<ore:dustLead>] , 500);
//recipies
recipes.removeShaped(<immersiveengineering:pickaxe_steel>);
recipes.removeShaped(<immersiveengineering:shovel_steel>);
recipes.removeShaped(<immersiveengineering:sword_steel>);
recipes.removeShaped(<immersiveengineering:axe_steel>);
recipes.removeShaped(<immersiveengineering:wooden_device0:0>);
recipes.removeShaped(<immersiveengineering:wooden_device0:5>);
recipes.removeShaped(<immersiveengineering:toolbox>);
recipes.addShaped(<immersiveengineering:toolbox>,
 [[null, null, null],
  [<ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>],
  [<ore:dyeRed>, <ore:plankTreatedWood>, <ore:dyeRed>]]);
//squeezer (addRecipe like horsepower)
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:grapejuice>*400, <ore:cropGrape>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:applejuice>*400, <minecraft:apple>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:applejuice>*500, <millenaire:ciderapple>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:honey>*400, <rustic:honeycomb>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:wildberryjuice>*400, <rustic:wildberries>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:ironberryjuice>*400, <rustic:ironberries>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:oliveoil>*400, <ore:cropOlive>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(null, <liquid:oliveoil>*400, <millenaire:olives>, 1024);
mods.immersiveengineering.Squeezer.addRecipe(<rustic:dust_tiny_iron>*3, <liquid:ironberryjuice>*500, <rustic:log:1>, 4096);
//"metal" press
mods.immersiveengineering.MetalPress.addRecipe(<millenaire:stone_deco:0>, <tconstruct:materials:1>, <millenaire:brickmould>, 512, 4);
mods.immersiveengineering.MetalPress.addRecipe(<minecraft:blaze_rod>, <minecraft:blaze_powder>, <immersiveengineering:mold:2>, 512, 6);
//stack resizing
val woodenbarrel = <immersiveengineering:wooden_device0:0>;
val metalbarrel = <immersiveengineering:metal_device0:4>;
val cap0 = <immersiveengineering:metal_device0:0>;
val cap1 = <immersiveengineering:metal_device0:1>;
val cap2 = <immersiveengineering:metal_device0:2>;
woodenbarrel.maxStackSize = 1;
metalbarrel.maxStackSize = 1;
cap0.maxStackSize = 1;
cap1.maxStackSize = 1;
cap2.maxStackSize = 1;

//alloy kiln (the buggers missed aluminum brass)
mods.immersiveengineering.AlloySmelter.addRecipe(<tconstruct:ingots:5>*4, <ore:ingotAluminum>*3, <ore:ingotCopper>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<tconstruct:ingots:5>*4, <ore:ingotAluminum>*3, <ore:dustCopper>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<tconstruct:ingots:5>*4, <ore:dustAluminum>*3, <ore:dustCopper>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<tconstruct:ingots:5>*4, <ore:dustAluminum>*3, <ore:ingotCopper>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<railcraft:ingot:9>*4, <ore:ingotCopper>*3, <ore:dustZinc>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<railcraft:ingot:9>*4, <ore:dustCopper>*3, <ore:dustZinc>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<railcraft:ingot:5>*4, <ore:ingotCopper>*3, <ore:dustTin>, 2000);
mods.immersiveengineering.AlloySmelter.addRecipe(<railcraft:ingot:5>*4, <ore:dustCopper>*3, <ore:dustTin>, 2000);
