//changed builders
recipes.removeShaped(<buildcraftbuilders:quarry>);
recipes.addShaped(<buildcraftbuilders:quarry>,
 [[<immersiveengineering:drillhead:0>, <ore:gearBrass>, <immersiveengineering:drillhead:0>],
  [<ore:gearBrass>, <immersiveengineering:metal_decoration0:5>, <ore:gearBrass>],
  [<ore:gearInvar>, <ore:gearBrass>, <ore:gearInvar>]]);
//changed tools
recipes.removeShapeless(<buildcraftlib:guide>);
recipes.removeShaped(<buildcraftcore:marker_connector>);
recipes.addShaped(<buildcraftcore:marker_connector>,
 [[null, <minecraft:redstone_torch>, null],
  [null, <ore:stickTreatedWood>, null],
  [null, <buildcraftcore:wrench>, null]]);
recipes.removeShaped(<buildcraftcore:wrench>);
recipes.addShaped(<buildcraftcore:wrench>,
 [[<ore:ingotIron>, null, <ore:ingotIron>],
  [null, <ore:ingotIron>, null],
  [null, <ore:ingotIron>, null]]);

