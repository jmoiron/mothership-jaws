//starting and growing your colony now depends a good deal on trade with Millenaire villagers
recipes.removeShaped(<minecolonies:supplychestdeployer>);
recipes.removeShaped(<minecolonies:supplycampdeployer>);
recipes.addShaped(<minecolonies:supplycampdeployer>,
 [[<millenaire:denieror>, <minecraft:hay_block>, <millenaire:denieror>],
  [<minecraft:bed>, <minecraft:flint_and_steel>, <minecraft:furnace>],
  [<ore:chest>, <astikorcarts:cargocart>, <ore:chest>]]);
recipes.removeShaped(<minecolonies:blockhutcitizen>);
recipes.addShaped(<minecolonies:blockhutcitizen>,
 [[<ore:plankWood>, <millenaire:denieror>, <ore:plankWood>],
  [<ore:plankWood>, <minecraft:torch>, <ore:plankWood>],
  [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]]);


