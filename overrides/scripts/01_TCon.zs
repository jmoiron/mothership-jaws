//oredict addition
val cropGrape = <ore:cropGrape>;
cropGrape.add(<millenaire:grapes>);
cropGrape.add(<rustic:grapes>);
val dustTin = <ore:dustTin>;
dustTin.add(<contenttweaker:tin_grit>);
val dustZinc = <ore:dustZinc>;
dustZinc.add(<contenttweaker:zinc_grit>);
//basically overworld drop + lava = nether drop
mods.tconstruct.Casting.addBasinRecipe(<minecraft:blaze_rod>, <immersiveengineering:material:0>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:netherrack>, <minecraft:cobblestone>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:soul_sand>, <ore:sand>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:nether_wart>, <ore:cropGrape>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:ghast_tear>, <minecraft:iron_nugget>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:glowstone_dust>, <ore:dustIron>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<tconstruct:edible:4>, <ore:slimeball>, <liquid:lava>, 1000, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<railcraft:firestone_raw>, <minecraft:quartz>, <liquid:lava>, 1000, true, 30);
recipes.addShapeless(<minecraft:netherbrick> * 4, [<minecraft:nether_brick>]);
//enderfication variant, same deal but with silver
mods.tconstruct.Casting.addBasinRecipe(<minecraft:chorus_fruit_popped>, <minecraft:apple>, <liquid:silver>, 144, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:end_stone>, <minecraft:cobblestone>, <liquid:silver>, 144, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecraft:slime_ball>, <ore:slimeball>, <liquid:silver>, 144, true, 30);
mods.tconstruct.Casting.addBasinRecipe(<minecolonies:ancienttome>, <minecraft:book>, <liquid:silver>, 144, true, 30);
//more basin recipes
mods.tconstruct.Casting.addBasinRecipe(<minecraft:dye:0>, <minecraft:coal>, <liquid:water>, 1000, true, 30);
//melting
mods.tconstruct.Melting.addRecipe(<liquid:iron> * 16,<rustic:dust_tiny_iron>);
//making the turbine rotor unbreakable
val rotor = <railcraft:turbine_rotor>;
val oprotor = rotor.withTag({Unbreakable:1});
mods.tconstruct.Casting.addBasinRecipe(oprotor, rotor, <liquid:constantan>, 2592, true, 3000);
//blast brick nonsense
recipes.removeByRecipeName("immersiveengineering:stone_decoration/blastbrick");
recipes.addShapeless(<immersiveengineering:stone_decoration:1>*3, [<tcomplement:scorched_block:3>, <tcomplement:scorched_block:3>, <tcomplement:scorched_block:3>, <minecraft:blaze_powder>]);
recipes.removeShaped(<railcraft:blast_furnace>);
recipes.replaceAllOccurences(<railcraft:blast_furnace>, <immersiveengineering:stone_decoration:1>);
//remove alt steel armor
recipes.removeShaped(<tcomplement:steel_helmet>);
recipes.removeShaped(<tcomplement:steel_chestplate>);
recipes.removeShaped(<tcomplement:steel_leggings>);
recipes.removeShaped(<tcomplement:steel_boots>);
//stubborn ceramics
val unfired = <ceramics:unfired_clay>;
unfired.maxStackSize = 32;
//disabling a few op things
recipes.removeShapeless(<tconstruct:throwball:1>);
recipes.removeShaped(<tconstruct:slime_boots:*>);
recipes.removeShaped(<tconstruct:slimesling:*>);
recipes.removeShaped(<tconstruct:wood_rail>);
recipes.removeShaped(<tconstruct:wood_rail_trapdoor>);