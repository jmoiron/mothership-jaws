#loader contenttweaker
import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Item;
import mods.contenttweaker.Block;

var tingrit = VanillaFactory.createItem("tin_grit");
tingrit.maxStackSize = 16;
tingrit.register();

var zincgrit = VanillaFactory.createItem("zinc_grit");
zincgrit.maxStackSize = 16;
zincgrit.register();

var roughautunite = VanillaFactory.createBlock("rough_autunite", <blockmaterial:rock>);
roughautunite.setBlockHardness(5.0);
roughautunite.setBlockResistance(8.0);
roughautunite.setToolClass("pickaxe");
roughautunite.setToolLevel(1);
roughautunite.setBlockSoundType(<soundtype:stone>);
roughautunite.register();

var roughbauxite = VanillaFactory.createBlock("rough_bauxite", <blockmaterial:rock>);
roughbauxite.setBlockHardness(5.0);
roughbauxite.setBlockResistance(8.0);
roughbauxite.setToolClass("pickaxe");
roughbauxite.setToolLevel(1);
roughbauxite.setBlockSoundType(<soundtype:stone>);
roughbauxite.register();

var roughcassiterite = VanillaFactory.createBlock("rough_cassiterite", <blockmaterial:rock>);
roughcassiterite.setBlockHardness(5.0);
roughcassiterite.setBlockResistance(8.0);
roughcassiterite.setToolClass("pickaxe");
roughcassiterite.setToolLevel(1);
roughcassiterite.setBlockSoundType(<soundtype:stone>);
roughcassiterite.register();

var roughcinnabar = VanillaFactory.createBlock("rough_cinnabar", <blockmaterial:rock>);
roughcinnabar.setBlockHardness(5.0);
roughcinnabar.setBlockResistance(8.0);
roughcinnabar.setToolClass("pickaxe");
roughcinnabar.setToolLevel(1);
roughcinnabar.setBlockSoundType(<soundtype:stone>);
roughcinnabar.register();

var roughgalena = VanillaFactory.createBlock("rough_galena", <blockmaterial:rock>);
roughgalena.setBlockHardness(5.0);
roughgalena.setBlockResistance(8.0);
roughgalena.setToolClass("pickaxe");
roughgalena.setToolLevel(1);
roughgalena.setBlockSoundType(<soundtype:stone>);
roughgalena.register();

var roughgold = VanillaFactory.createBlock("rough_gold", <blockmaterial:rock>);
roughgold.setBlockHardness(5.0);
roughgold.setBlockResistance(8.0);
roughgold.setToolClass("pickaxe");
roughgold.setToolLevel(1);
roughgold.setBlockSoundType(<soundtype:stone>);
roughgold.register();

var roughhematite = VanillaFactory.createBlock("rough_hematite", <blockmaterial:rock>);
roughhematite.setBlockHardness(5.0);
roughhematite.setBlockResistance(8.0);
roughhematite.setToolClass("pickaxe");
roughhematite.setToolLevel(1);
roughhematite.setBlockSoundType(<soundtype:stone>);
roughhematite.register();

var roughlapis = VanillaFactory.createBlock("rough_lapis", <blockmaterial:rock>);
roughlapis.setBlockHardness(5.0);
roughlapis.setBlockResistance(8.0);
roughlapis.setToolClass("pickaxe");
roughlapis.setToolLevel(1);
roughlapis.setBlockSoundType(<soundtype:stone>);
roughlapis.register();

var roughlimonite = VanillaFactory.createBlock("rough_limonite", <blockmaterial:rock>);
roughlimonite.setBlockHardness(5.0);
roughlimonite.setBlockResistance(8.0);
roughlimonite.setToolClass("pickaxe");
roughlimonite.setToolLevel(1);
roughlimonite.setBlockSoundType(<soundtype:stone>);
roughlimonite.register();

var roughmalachite = VanillaFactory.createBlock("rough_malachite", <blockmaterial:rock>);
roughmalachite.setBlockHardness(5.0);
roughmalachite.setBlockResistance(8.0);
roughmalachite.setToolClass("pickaxe");
roughmalachite.setToolLevel(1);
roughmalachite.setBlockSoundType(<soundtype:stone>);
roughmalachite.register();

var roughsphalerite = VanillaFactory.createBlock("rough_sphalerite", <blockmaterial:rock>);
roughsphalerite.setBlockHardness(5.0);
roughsphalerite.setBlockResistance(8.0);
roughsphalerite.setToolClass("pickaxe");
roughsphalerite.setToolLevel(1);
roughsphalerite.setBlockSoundType(<soundtype:stone>);
roughsphalerite.register();

var roughteallite = VanillaFactory.createBlock("rough_teallite", <blockmaterial:rock>);
roughteallite.setBlockHardness(5.0);
roughteallite.setBlockResistance(8.0);
roughteallite.setToolClass("pickaxe");
roughteallite.setToolLevel(1);
roughteallite.setBlockSoundType(<soundtype:stone>);
roughteallite.register();

var roughcoal = VanillaFactory.createBlock("rough_coal", <blockmaterial:rock>);
roughcoal.setBlockHardness(5.0);
roughcoal.setBlockResistance(8.0);
roughcoal.setToolClass("pickaxe");
roughcoal.setToolLevel(1);
roughcoal.setBlockSoundType(<soundtype:stone>);
roughcoal.register();