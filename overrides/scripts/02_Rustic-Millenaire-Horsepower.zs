recipes.removeShaped(<millenaire:winebasic>);
recipes.removeShaped(<millenaire:cider>);
recipes.removeShaped(<millenaire:oliveoil>);

mods.rustic.CrushingTub.addRecipe(<liquid:grapejuice>*250, null, <millenaire:grapes>);
mods.rustic.CrushingTub.addRecipe(<liquid:applejuice>*400, null, <millenaire:ciderapple>);
mods.rustic.CrushingTub.addRecipe(<liquid:oliveoil>*400, null, <millenaire:olives>);

mods.horsepower.Press.add(<millenaire:grapes>, <liquid:grapejuice>*250);
mods.horsepower.Press.add(<rustic:grapes>, <liquid:grapejuice>*250);
mods.horsepower.Press.add(<minecraft:apple>, <liquid:applejuice>*250);
mods.horsepower.Press.add(<millenaire:ciderapple>, <liquid:applejuice>*400);
mods.horsepower.Press.add(<rustic:honeycomb>, <liquid:honey>*250);
mods.horsepower.Press.add(<rustic:wildberries>, <liquid:wildberryjuice>*250);
mods.horsepower.Press.add(<rustic:ironberries>, <liquid:ironberryjuice>*250);
mods.horsepower.Press.add(<rustic:olives>, <liquid:oliveoil>*250);
mods.horsepower.Press.add(<millenaire:olives>, <liquid:oliveoil>*250);
mods.horsepower.Press.add(<minecraft:reeds>, <minecraft:sugar>*2);

val booze = <rustic:fluid_bottle>;
val wine = booze.withTag({Fluid:{FluidName:"wine",Amount:1000}});
val ironwine = booze.withTag({Fluid:{FluidName:"ironwine",Amount:1000}});
val wildberrywine = booze.withTag({Fluid:{FluidName:"wildberrywine",Amount:1000}});
val cider = booze.withTag({Fluid:{FluidName:"cider",Amount:1000}});
val oliveoil = booze.withTag({Fluid:{FluidName:"oliveoil",Amount:1000}});

recipes.addShapeless(<millenaire:winebasic>, [<minecraft:paper>, wine]);
recipes.addShapeless(<millenaire:winebasic>, [<minecraft:paper>, wildberrywine]);
recipes.addShapeless(<millenaire:winebasic>, [<minecraft:paper>, ironwine]);
recipes.addShapeless(<millenaire:cider>, [<minecraft:paper>, cider]);
recipes.addShapeless(<millenaire:oliveoil>, [<minecraft:paper>, oliveoil]);