//odds and ends
recipes.removeShaped(<parachutemod:parachute>);
recipes.addShaped(<parachutemod:parachute>,
 [[<ore:fabricHemp>, <ore:fabricHemp>, <ore:fabricHemp>],
  [<minecraft:string>, null, <minecraft:string>],
  [null, <minecraft:leather>, null]]);
recipes.removeShaped(<astikorcarts:wheel>);
recipes.addShaped(<astikorcarts:wheel>,
 [[<minecraft:stick>, <minecraft:stick>, <minecraft:stick>],
  [<minecraft:stick>, <ore:gearBushing>, <minecraft:stick>],
  [<minecraft:stick>, <minecraft:stick>, <minecraft:stick>]]);
recipes.removeShaped(<flopper:flopper>);
recipes.addShaped(<flopper:flopper>,
 [[null, null, null],
  [<ore:ingotInvar>, <minecraft:bucket>, <ore:ingotInvar>],
  [null, <ore:ingotInvar>, null]]);
recipes.addShaped(<minecraft:saddle>,
 [[<minecraft:leather>, null, <minecraft:leather>],
  [<minecraft:leather>, <minecraft:leather>, <minecraft:leather>],
  [null, <ore:ingotIron>, null]]);
recipes.addShapeless(<minecraft:skull:1>, [<minecraft:skull:0>, <ore:dyeBlack>]);
recipes.addShapeless(<rtfm:book_manual>, [<minecraft:book>, <quark:horse_whistle>]);
recipes.removeShaped(<rustic:crop_stake>);
recipes.addShaped(<rustic:crop_stake>,
 [[null, null, <ore:plankWood>],
  [null, <ore:plankWood>, null],
  [<ore:plankWood>, null, null]]);
recipes.removeShaped(<inspirations:mulch:0>);
recipes.addShaped(<inspirations:mulch:0>,
 [[null, <minecraft:stick>, null],
  [<minecraft:stick>, null, <minecraft:stick>],
  [null, <minecraft:stick>, null]]);
recipes.removeShaped(<minecraft:purple_shulker_box>);
//furnace
furnace.addRecipe(<railcraft:ingot:8>, <ore:dustZinc>);
furnace.addRecipe(<railcraft:ingot:2>, <ore:dustTin>);