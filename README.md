# Mothership JAWS pack

This is an unofficial extension to an existing pack meant to be played
on a private server.  The author of the upstream pack did not want to
sanction a public project related to their own, so a fork has been made
according to the license and we've made an attempt to distance this
from that project to prevent confusion.

License: [MIT](https://opensource.org/licenses/MIT)

## What is this

This shared repository allows us to collaborate on file modifications,
share tools, and track changes to the pack over time.

Bitbucket is like github, except (for me) it's more discrete.

## Working on the pack

Use git:

```
git clone git@bitbucket.org:jmoiron/mothership-jaws.git
```

Or, for windows/mac:

Use [sourcetree](https://www.sourcetreeapp.com/)
