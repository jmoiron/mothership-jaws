#!/usr/bin/env bash

VERSION=$(cat manifest.json | jq -r .version)
NAME="Mothership JAWS edition-${VERSION}.zip"

zip "$NAME" -9 -r manifest.json overrides
