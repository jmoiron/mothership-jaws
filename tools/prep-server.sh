#!/usr/bin/env bash

set -eo pipefail

# directory with this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
VERSION=$(cat manifest.json | jq -r .version)
NAME="Mothership JAWS edition-${VERSION}-server.zip"

MCPATH="$1"

if [ -z "$MCPATH" ]; then
    echo "first, load pack via a launcher like multiMC (but do not launch it)"
    echo "usage: prep-server.sh <path-to-minecraft-folder>"
fi

EXCLUDE="${DIR}/exclude.lst"

ZIP_PATH="$(pwd)/${NAME}"
echo $ZIP_PATH

rm serverfiles/server.properties
sed "s/{VERSION}/${VERSION}/g" serverfiles/server.properties.tpl | tee serverfiles/server.properties

pushd serverfiles
zip "${ZIP_PATH}" -9 -x "@${EXCLUDE}" -r *
popd

pushd "$MCPATH" 
zip "${ZIP_PATH}" -9 -x "@${EXCLUDE}" -r *
popd

