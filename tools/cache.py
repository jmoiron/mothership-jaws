#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""cache.py maintains a set of caches of twitchapp API results"""

import os
import shutil

import diskcache as dc

xdg_data_home = os.getenv("XDG_DATA_HOME")
if not xdg_data_home:
    xdg_data_home = os.path.expanduser("~/.local/share")

cache_path = os.path.join(xdg_data_home, "twitchapp.cache")
if not os.path.isdir(cache_path):
    os.makedirs(cache_path)


class Cache(object):
    def __init__(self, path=cache_path):
        self.path = path
        self.c = dc.Cache(self.path)
        # stats
        self.hits = 0
        self.misses = 0
        self.puts = 0

    def hit(self, hit):
        if hit:
            self.hits += 1
            return
        self.misses += 1

    def get(self, key):
        data = self.c.get(key)
        self.hit(bool(data))
        return data

    def put(self, key, value):
        self.c[key] = value
        self.puts += 1

    def stats(self):
        return {
            "gets": self.hits + self.misses,
            "hits": self.hits,
            "hit_pct": float(self.hits * 100) / (self.hits + self.misses),
            "misses": self.misses,
            "puts": self.puts,
        }

    def close(self):
        self.c.close()

    def clear(self):
        shutil.rmtree(self.path)
