#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Looks for updates to mods in manifest.json."""

import json
import requests
import argparse
import os
import cache
import pprint
import time
import datetime
import urllib.parse

from multiprocessing.pool import ThreadPool

TWITCHAPP_BASE = "https://addons-ecs.forgesvc.net/api/v2/"

white, black, red, green, yellow, blue, purple = range(89, 96)


def color(string, color=green, bold=False):
    """Usage: color("foo", red, bold=True)"""
    return "\033[%s%sm" % ("01;" if bold else "", color) + str(string) + "\033[0m"


class Timer(object):
    def __init__(self):
        self.t0 = time.time()
        self.prev = self.t0

    def tick(self):
        now = time.time()
        dt = now - self.prev
        self.prev = now
        return dt

    def total(self):
        return time.time() - self.t0


def addon_url(*strings):
    return os.path.join(TWITCHAPP_BASE, "addon", *strings)


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("path", help="manifest.json path", default="./manifest.json", nargs="?")
    parser.add_argument("--search", type=str, help="search for a mod to add to the manifest")
    parser.add_argument("--clear", action="store_true", help="clear cache and exit")
    parser.add_argument("--no-cache", action="store_true", help="do not use cache (mostly.)")
    parser.add_argument("--updates-only", action="store_true", help="only print out mods with updates")
    parser.add_argument("--format", action="store_true", help="format the manifest file but do not alter")
    parser.add_argument("--modify", action="store_true", help="modify the manifest file")
    parser.add_argument("--version", type=str, help="override version for new manifest")
    parser.add_argument("--bump", action="store_true", help="auto-bump version of manifest and take no other actions")
    parser.add_argument("--add-mod", type=str, help="add a mod to the manifest by ID (see --search)")
    parser.add_argument("--no-sort", action="store_true", help="do not sort mod info output; keep manifest order")
    parser.add_argument("--skip", nargs="+", help="skip mods to update by mod id")
    args = parser.parse_args()
    args.path = os.path.expanduser(args.path)
    return args


def read_manifest(path):
    with open(path) as f:
        return json.load(f)


def parse_date(string):
    formats = (
        "%Y-%m-%dT%H:%M:%S.%fZ",  # "2018-08-28T05:27:19.65Z",
        "%Y-%m-%dT%H:%M:%SZ",  # "2018-08-28T05:27:19Z",
    )
    strp = datetime.datetime.strptime
    for format in formats:
        try:
            return strp(string, format)
        except ValueError:
            pass
    print("Unknown format: %s" % (string))


def date(dt):
    return dt.strftime("%Y-%m-%d")


def age(td):
    if td.days > 2:
        return str(td).split(",")[0]
    return str(td).split(".")[0]


class Client(object):
    def __init__(self):
        self.cache = cache.Cache()
        self.session = requests.Session()
        self.session.headers.update({"User-Agent": "Mozilla/5.0"})
        self.verbose = True

    def geturl(self, url):
        if self.verbose:
            print("fetch > %s" % (url))
        return self.session.get(url)

    def get_file_info(self, f, use_cache=True):
        # files are immutable so we should always use_cache=True
        proj = str(f["projectID"])
        id = str(f["fileID"])
        url = addon_url(proj, "file", id)

        data = self.cache.get(url)
        if data:
            return (f, url, json.loads(data))

        resp = self.geturl(url)
        # lets try storing text in the cache so it isn't super slow
        self.cache.put(url, resp.text)
        return (f, url, resp.json())

    def get_file_project_info(self, f, use_cache=True):
        # gets project and file info
        # files are immutable so we will always use cache there
        _, _, file_info = self.get_file_info(f)

        info = {"file": file_info}

        proj = str(f["projectID"])
        url = addon_url(proj)

        data = self.cache.get(url)
        if data:
            info["project"] = json.loads(data)
            return (f, info)

        resp = self.geturl(url)
        self.cache.put(url, resp.text)
        info["project"] = resp.json()
        return (f, info)

    def get_project_files(self, project_id, use_cache=True):
        # gets files for project id
        url = addon_url(project_id, "files")
        data = self.cache.get(url)
        if data:
            return (project_id, json.loads(data))

        resp = self.geturl(url)
        self.cache.put(url, resp.text)
        data = resp.json()
        return (project_id, data)

    def search(self, name):
        url = addon_url("search")
        params = {
            "categoryID": 0,
            "gameID": 432,
            "searchFilter": name,
        }
        url += "?%s" % (urllib.parse.urlencode(params))
        return self.geturl(url)

    def get_project(self, id, use_cache=True):
        url = addon_url(id)
        data = self.cache.get(url)
        if data:
            return (id, json.loads(data))

        resp = self.geturl(url)
        self.cache.put(url, resp.text)
        return (id, resp.json())


def format(path):
    data = read_manifest(path)
    write_manifest(data, path)


def write_manifest(data, path):
    with open(path, "w") as f:
        json.dump(data, f, indent=2)


def incr_version(version):
    version = list(map(int, version.split(".")))
    version[-1] += 1
    return ".".join(map(str, version))


def main():
    args = parse_args()
    t = Timer()

    manifest = read_manifest(args.path)
    target_version = manifest["minecraft"]["version"]

    dr = t.tick()

    c = Client()
    if args.clear:
        c.cache.clear()
        return

    if args.search:
        resp = c.search(args.search)
        results = resp.json()
        for res in results:
            print("%s %s\n  %s" % (color(res["id"], bold=True), res["name"], color(res["websiteUrl"], blue)))
        return

    if args.bump:
        manifest["version"] = incr_version(manifest["version"])
        write_manifest(manifest, args.path)
        return

    if args.add_mod:
        _, data = c.get_project(args.add_mod)

        files = data["latestFiles"]
        files = [f for f in files if target_version in f["gameVersion"]]

        if not files:
            # we didn't find a file of the right version in the project summary
            # but it's possible that we're looking for an old version that isn't
            # there anymore, so lets check the full file list for the mod
            versions = []
            for f in data["latestFiles"]:
                versions += f["gameVersion"]
            versions = sorted(list(set(versions)))
            versions_str = ",".join(versions)
            # print("No file found for version %s in latest (found %s)" % (target_version, versions_str))
            _, files = c.get_project_files(args.add_mod)

            files = [f for f in files if target_version in f["gameVersion"]]

        files.sort(key=lambda f: f["releaseType"])

        if not files:
            print("No files available for version %s" % (target_version))
            return

        # take the highest file
        f = files[0]
        print("adding %s {%s} for project %s {%s}" % (f["displayName"], f["id"], data["name"], data["id"]))
        manifest["files"].append(
            {"projectID": data["id"], "fileID": f["id"], "required": True,}
        )

        write_manifest(manifest, args.path)
        return

    pool = ThreadPool(5)
    files = manifest["files"]
    results = pool.map(c.get_file_project_info, files)
    dres = t.tick()

    if args.format:
        format(args.path)
        return

    args.skip = args.skip or []
    args.skip = [int(a) for a in args.skip]
    updates = []

    if not args.no_sort:
        results.sort(key=lambda f: f[1]["project"]["name"])

    for _, info in results:
        newest = None
        p, f = info["project"], info["file"]

        if int(p["id"]) in args.skip:
            continue

        for x in p["latestFiles"]:
            if target_version not in x["gameVersion"]:
                continue
            # only use final releases
            if x["releaseType"] > f["releaseType"]:
                continue
            if parse_date(x["fileDate"]) > parse_date(f["fileDate"]):
                newest = dict(x)

        if not newest:
            if not args.updates_only:
                print(" %s %s (%s) (project=%d)" % (color("*"), p["name"], f["fileName"], p["id"]))
        else:
            # pprint.pprint(f)
            xfd = parse_date(newest["fileDate"])
            fd = parse_date(f["fileDate"])
            updates.append((p, newest))
            if args.updates_only:
                print(
                    "%s %s\n    %s\n %s %s\n    updated on %s, %s newer\n    %s <%s>"
                    % (
                        color(p["name"]),
                        color(p["websiteUrl"], blue),
                        f["fileName"],
                        color("->"),
                        color(newest["fileName"], white, bold=True),
                        date(xfd),
                        age(xfd - fd),
                        newest["downloadUrl"],
                        newest["id"],
                    )
                )
            else:
                print(" %s %s (%s) (project=%d)" % (color("*", red), p["name"], f["fileName"], p["id"]))
                print("    update: %s (%s, %s)" % (color(x["fileName"], color=white, bold=True), xfd, xfd - fd))

    if args.modify and len(updates):
        if not args.version:
            args.version = incr_version(manifest["version"])
        print("updating %d mods, setting version to %s" % (len(updates), args.version))
        umap = {}
        for u, new in updates:
            pid = u["id"]
            fid = new["id"]
            if int(pid) not in args.skip:
                umap[pid] = fid
        for f in manifest["files"]:
            if f["projectID"] in umap:
                f["fileID"] = umap[f["projectID"]]

        manifest["version"] = args.version
        write_manifest(manifest, args.path)

    # print(c.cache.stats())
    # print("read=%s res=%s total=%s" % (dr, dres, t.total()))


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
