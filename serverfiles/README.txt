After first launching the server, go into the eula.txt file and change "false" to "true."
Also, be sure to adjust the RAM settings for your specific computer or else the server won't run quite right.
You can change the terrain generator in the server.properties file. By default, it's OTG Biome Bundle.
